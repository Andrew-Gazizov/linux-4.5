/*
 *  Copyright (C) 2016 MDB Compas
 *	Andrew Gazizov, MDB Compas, <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/clk/p2s_mscu.h>
#include <linux/of.h>

#include "mscu.h"

#define AMBA_DIV_MASK	0x07

struct clk_abma_characteristics {
	struct clk_range output;
	u32 divisors[5];
};

#define to_clk_amba(hw) container_of(hw, struct clk_amba, hw)

struct clk_amba {
	struct clk_hw hw;
	const struct clk_amba_characteristics *characteristics;
};

static unsigned long clk_amba_recalc_rate(struct clk_hw *hw,
					    unsigned long parent_rate)
{
	u8 div;
	unsigned long rate = parent_rate;
	struct clk_amba *amba = to_clk_amba(hw);
	const struct clk_abma_characteristics *characteristics =
			amba->characteristics;

	div = (p2s_mscu_read(P2S_MSCU_CLKDIV) >> 4) & AMBA_DIV_MASK;

	rate /= characteristics->divisors[div];

	if (rate < characteristics->output.min)
		pr_warn("amba is underclocked");
	else if (rate > characteristics->output.max)
		pr_warn("amba is overclocked");
	return rate;
}

static const struct clk_ops clk_amba_ops = {
	.recalc_rate = clk_amba_recalc_rate,
};

static struct clk * __init p2s_clk_amba_register(const char *name,
		const char *parent_name,
		const struct clk_amba_characteristics *characteristics)
{
	struct clk_amba *cpu;
	struct clk *clk = NULL;
	struct clk_init_data init;

	if (!name || !parent_name)
		return ERR_PTR(-EINVAL);

	cpu = kzalloc(sizeof(*cpu), GFP_KERNEL);
	if (!cpu)
		return ERR_PTR(-ENOMEM);

	init.name = name;
	init.ops = &clk_amba_ops;
	init.parent_names = &parent_name;
	init.num_parents = 1;
	init.flags = 0;

	cpu->hw.init = &init;
	cpu->characteristics = characteristics;

	clk = clk_register(NULL, &cpu->hw);
	if (IS_ERR(clk))
		kfree(cpu);

	return clk;
}

static struct clk_abma_characteristics * __init
of_p2s_clk_amba_get_characteristics(struct device_node *np)
{
	struct clk_abma_characteristics *characteristics;

	characteristics = kzalloc(sizeof(*characteristics), GFP_KERNEL);
	if (!characteristics)
		return NULL;

	if (of_p2s_get_clk_range(np, "compas,clk-output-range", &characteristics->output))
		goto out_free_characteristics;

	of_property_read_u32_array(np, "compas,clk-divisors",
				   characteristics->divisors, 5);

	return characteristics;

out_free_characteristics:
	kfree(characteristics);
	return NULL;
}

void __init of_p2s0927a2_clk_amba_setup(struct device_node *np)
{
	struct clk *clk;
	const char *parent_name;
	const char *name = np->name;
	struct clk_amba_characteristics *characteristics;

	parent_name = of_clk_get_parent_name(np, 0);

	of_property_read_string(np, "clock-output-names", &name);

	characteristics = of_p2s_clk_amba_get_characteristics(np);
	if (!characteristics)
		return;

	clk = p2s_clk_amba_register(name, parent_name, characteristics);
	if (IS_ERR(clk))
		goto out_free_characteristics;

	of_clk_add_provider(np, of_clk_src_simple_get, clk);
	return;

out_free_characteristics:
	kfree(characteristics);
}
