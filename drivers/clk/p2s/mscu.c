/*
 *  Copyright (C) 2016 MDB Compas
 *	Andrew Gazizov, MDB Compas, <gazizovandrey@live.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/clk/p2s_mscu.h>
#include <linux/of.h>
#include <linux/of_address.h>

#include "mscu.h"

void __iomem *p2s_mscu_base;

int of_p2s_get_clk_range(struct device_node *np, const char *propname,
			  struct clk_range *range)
{
	u32 min, max;
	int ret;

	ret = of_property_read_u32_index(np, propname, 0, &min);
	if (ret)
		return ret;

	ret = of_property_read_u32_index(np, propname, 1, &max);
	if (ret)
		return ret;

	if (range) {
		range->min = min;
		range->max = max;
	}

	return 0;
}
EXPORT_SYMBOL_GPL(of_p2s_get_clk_range);

static const struct of_device_id mscu_clk_ids[] __initconst = {
	{
		.compatible = "compas,p2s0927a2-clk-pll",
		.data = of_p2s0927a2_clk_pll_setup,
	}, {
		.compatible = "compas,p2s0927a2-clk-cpu",
		.data = of_p2s0927a2_clk_cpu_setup,
	}, {
		.compatible = "compas,p2s0927a2-clk-amba",
		.data = of_p2s0927a2_clk_amba_setup,
	}, {
		.compatible = "compas,p2s0927a2-clk-peripheral",
		.data = of_p2s0927a2_clk_periph_setup,
	},
	{ /*sentinel*/ }
};

static void __init of_p2s0927a2_mscu_setup(struct device_node *np)
{
	struct device_node *childnp;
	void (*clk_setup)(struct device_node *);
	const struct of_device_id *clk_id;

	p2s_mscu_base = of_iomap(np, 0);
	if (!p2s_mscu_base)
		return;

	/* Disable all clocks except mDDR and UART's */
	p2s_mscu_write(P2S_MSCU_CLKCFG, ~(P2S_MSCU_MDDR
			| P2S_MSCU_UART0 | P2S_MSCU_UART1 | P2S_MSCU_UART2));

	for_each_child_of_node(np, childnp) {
		clk_id = of_match_node(mscu_clk_ids, childnp);
		if (!clk_id)
			continue;
		clk_setup = clk_id->data;
		clk_setup(childnp);
	}

	/* Mux all configurable ports as GPIO's */
	p2s_mscu_write(P2S_MSCU_GPIOCFG, P2S_MSCU_MUX_MAC | P2S_MSCU_MUX_UART2 |
			P2S_MSCU_MUX_I2S | P2S_MSCU_MUX_CAN);
}

CLK_OF_DECLARE(p2s0927a2_clk_mscu, "compas,p2s0927a2-mscu",
		of_p2s0927a2_mscu_setup);
