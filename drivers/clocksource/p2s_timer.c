/*
 * linux/drivers/clocksource/p2s_timer.c
 *
 *  Copyright (C) 2016 MDB Compas
 *
 *	Andrew Gazizov   <gazizovandrey@live.ru>
 *  				 <a.gazizov@mkb-kompas.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/clk.h>

#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/clockchips.h>
#include <linux/bitops.h>

#define P2S_TMR_LR(n)	(n * 0x10)
#define P2S_TMR_CVR(n)	(n * 0x10 + 4)
#define P2S_TMR_CONR(n)	(n * 0x10 + 8)

#define P2S_TMR_EN		BIT(10)
#define P2S_TMR_TRIG	BIT(9)
#define P2S_TMR_SW		BIT(8)
#define P2S_TMR_MODE	BIT(7)
#define P2S_TMR_INTMASK	BIT(3)
#define P2S_TMR_INTCLR	BIT(2)
#define P2S_TMR_INTCON	BIT(1)

#define P2S_TMR_MODER	0x30

#define timer_readl(reg) readl_relaxed(timer_base + (reg))
#define timer_writel(val, reg) writel_relaxed((val), timer_base + (reg))

static int timer_latch;
static void __iomem *timer_base;

static cycle_t p2s_read_timer0(struct clocksource *cs)
{
	return ~timer_readl(P2S_TMR_CVR(0));
}

static irqreturn_t p2s0927a2_timer_interrupt(int irq, void *dev_id)
{
	struct clock_event_device *c = dev_id;

	u32 ctrl = timer_readl(P2S_TMR_CONR(1));
	writel(ctrl & ~P2S_TMR_INTCLR, timer_base + P2S_TMR_CONR(1));

	c->event_handler(c);
 	return IRQ_HANDLED;
}

static struct clocksource timer0 = {
	.name		= "timer0",
	.rating		= 300,
	.read		= p2s_read_timer0,
	.mask		= CLOCKSOURCE_MASK(32),
	.flags		= CLOCK_SOURCE_IS_CONTINUOUS,
};

static int p2s_timer1_shutdown(struct clock_event_device *evt)
{
	writel(0, timer_base + P2S_TMR_CONR(1));
	return 0;
}

static int p2s_timer1_set_periodic(struct clock_event_device *evt)
{
	writel(0, timer_base + P2S_TMR_CONR(1));
	/* Setup timer for periodic irqs; fixed rate of 1/HZ */
	writel(timer_latch - 1, timer_base + P2S_TMR_LR(1));
	writel(P2S_TMR_INTCON | P2S_TMR_SW | P2S_TMR_MODE,
			timer_base + P2S_TMR_CONR(1));
	return 0;
}

static int
p2s_timer1_next_event(unsigned long delta, struct clock_event_device *dev)
{
	if (delta == 0)
		return -ETIME;

	writel(delta, timer_base + P2S_TMR_LR(1));
	writel(P2S_TMR_INTCON | P2S_TMR_SW | P2S_TMR_MODE,
	       timer_base + P2S_TMR_CONR(1));
	return 0;
}

static struct clock_event_device clkevt = {
	.name		= "p2s_tick",
	.features		= CLOCK_EVT_FEAT_PERIODIC |
				  	  CLOCK_EVT_FEAT_ONESHOT,
	.rating		= 150,
	.set_next_event		= p2s_timer1_next_event,
	.set_state_shutdown	= p2s_timer1_shutdown,
	.set_state_periodic	= p2s_timer1_set_periodic,
};

void __init p2s_timer_dt_init(struct device_node *node)
{
	struct clk *tclk;
	unsigned long tclk_rate;
	int irq, ret;

	timer_base = of_iomap(node, 0);
	if (!timer_base)
		panic(pr_fmt("Could not map TIMER address\n"));

	/* Reset all timers */
	timer_writel(0, P2S_TMR_CONR(0));
	timer_writel(0, P2S_TMR_CONR(1));
	timer_writel(0, P2S_TMR_CONR(2));
	timer_writel(0, P2S_TMR_MODE);

	/* Get the interrupts property */
	irq  = irq_of_parse_and_map(node, 0);
	if (!irq)
		panic(pr_fmt("Unable to get IRQ from DT\n"));

	/* Make IRQs happen for the system timer */
	ret = request_irq(irq, p2s0927a2_timer_interrupt,
			IRQF_TIMER | IRQF_IRQPOLL, "p2s_tick", &clkevt);

	if (ret)
		panic(pr_fmt("Unable to setup IRQ\n"));

	tclk = of_clk_get(node, 0);
	if (IS_ERR(tclk))
		panic(pr_fmt("Unable to get timer clock\n"));

	clk_prepare_enable(tclk);
	if (ret)
		panic(pr_fmt("Could not enable timer clock\n"));

	tclk_rate = clk_get_rate(tclk);
	if (!tclk_rate)
		panic(pr_fmt("Invalid timer clock rate\n"));

	timer_latch = (tclk_rate + HZ/2) / HZ;
	timer_writel(0xffffffffu, P2S_TMR_LR(0));
	timer_writel(P2S_TMR_INTCON | P2S_TMR_SW | P2S_TMR_INTMASK,
			P2S_TMR_CONR(0));

	/* Setup timer clockevent */
	clkevt.cpumask = cpumask_of(0);
	clockevents_config_and_register(&clkevt, tclk_rate, 1, 0xffffffff - 1);

	/* register clocksource */
	clocksource_register_hz(&timer0, tclk_rate);
}

CLOCKSOURCE_OF_DECLARE(compas_timer, "compas,p2s-timer", p2s_timer_dt_init);
